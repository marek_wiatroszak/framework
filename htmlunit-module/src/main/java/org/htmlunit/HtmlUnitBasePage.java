package org.htmlunit;

import java.io.IOException;
import java.net.MalformedURLException;

import org.abstracts.IBasePage;

import com.gargoylesoftware.htmlunit.FailingHttpStatusCodeException;
import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.html.HtmlPage;

/**
 * This is the basis for the HTML abstract layer.
 * 
 * @author nazps
 * 
 */
public class HtmlUnitBasePage implements IBasePage {
	protected WebClient wc;
	protected String pageAddress;
	protected HtmlPage htmlPage;

	public HtmlUnitBasePage(WebClient _wc, String pageUrl) throws IOException {
		this.wc = _wc;
		this.pageAddress = pageUrl;
	}

	public String getTitle() {
		return htmlPage.getTitleText();
	}

	public HtmlUnitBasePage navigateToMe() {
		try {
			htmlPage = (HtmlPage) wc.getPage(getPageAddress());
		
		} catch (FailingHttpStatusCodeException e) {
			e.printStackTrace();
			return null;
		} catch (MalformedURLException e) {
			e.printStackTrace();
			return null;
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
		return this;

	}

	public String getPageAddress() {
		return this.pageAddress;
	}

	/**
	 * This method needs to be called after a button or link was clicked or some
	 * Ajax magic is happening so that the state of the page gets updated.
	 * 
	 * @param _htmlPage
	 */
	@SuppressWarnings({ "hiding", "unchecked" })
	public <HtmlPage> HtmlPage setPageState(HtmlPage _htmlPage) {
		this.htmlPage = (com.gargoylesoftware.htmlunit.html.HtmlPage) _htmlPage;
		return ((HtmlPage) htmlPage);
	}


}
