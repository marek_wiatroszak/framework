package org.htmlunit.engine;

import org.abstracts.IEngine;
import org.htmlunit.enchancements.QuietCssErrorHandler;
import org.htmlunit.enchancements.SilentIncorrectnessListener;

import com.gargoylesoftware.htmlunit.WebClient;

public class HtmlUnitEngine implements IEngine{
private WebClient webClient;
private boolean isStarted = false;

public void startEngine(){
	webClient = new WebClient();
	webClient.setIncorrectnessListener(new SilentIncorrectnessListener());
	webClient.setCssErrorHandler(new QuietCssErrorHandler());
	isStarted = true;
}

public void stopEngine() {
	clearSessionData();
	webClient.closeAllWindows();	
}

public void clearSessionData() {
	webClient.getCache().clear();
	webClient.getCookieManager().clearCookies();	
}
public WebClient getWebClient(){
	return this.webClient;
}

@Override
public boolean isStarted() {
	return isStarted;
}
}
