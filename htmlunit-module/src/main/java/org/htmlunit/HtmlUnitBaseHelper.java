package org.htmlunit;

import org.abstracts.IHelper;

import com.gargoylesoftware.htmlunit.WebClient;

public class HtmlUnitBaseHelper implements IHelper{
	protected WebClient wc;
	
	public HtmlUnitBaseHelper(WebClient _wc, String basePageUrl){
		this.wc = _wc;
	}
}
