package org.htmlunit.commons;

import java.io.IOException;

import org.abstracts.products.commons.IWelcomeCentrePage;
import org.htmlunit.HtmlUnitBasePage;

import com.gargoylesoftware.htmlunit.WebClient;

/**
 * This class provides abstract to Taleo/Oracle Welcome Center page. This page
 * object is strictly an example. Author of this page object does not own any
 * rights to the page reflected in this abstract. This page object may be
 * deleted at any time (Use at own risk)
 * 
 * @author nazps
 * 
 */
public class WelcomeCentrePage extends HtmlUnitBasePage implements IWelcomeCentrePage{

	public WelcomeCentrePage(WebClient _wc, String pageUrl) throws IOException {
		super(_wc, pageUrl);
	}

}
