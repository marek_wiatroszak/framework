package org.htmlunit.commons;

import java.io.IOException;

import org.abstracts.products.commons.helpers.ILoginHelper;
import org.htmlunit.HtmlUnitBaseHelper;

import com.gargoylesoftware.htmlunit.ElementNotFoundException;
import com.gargoylesoftware.htmlunit.WebClient;

public class LoginHelper extends HtmlUnitBaseHelper implements ILoginHelper {

	LoginPage loginPage;

	public LoginHelper(WebClient _wc, String basePageUrl) throws IOException {
		super(_wc, basePageUrl);
		loginPage = new LoginPage(this.wc, basePageUrl);
	}

	/**
	 * Navigate to legal agreements to login page and logins to the given credentials. Clears all
	 * session data
	 * 
	 * @param username
	 * @param password
	 * @throws IOException
	 */
	public WelcomeCentrePage login(String username, String password)
			throws IOException {
		this.wc.getCache().clear();
		this.wc.getCookieManager().clearCookies();
		loginPage.navigateToMe();
		loginPage.getLoginField().type(username);
		loginPage.getPasswordField().type(password);
		WelcomeCentrePage wcp = new WelcomeCentrePage(this.wc, loginPage.getPageAddress());
		wcp.setPageState(loginPage.getSubmitButton().click());
		return wcp;
	}

	/**
	 * This method returns text content of the error message displayed after invalid credentials has been entered.
	 * 
	 * @param username
	 * @param password
	 * @return
	 * @throws IOException
	 */
	public String invalidLogin(String username, String password)
			throws IOException {
		this.wc.getCache().clear();
		this.wc.getCookieManager().clearCookies();
		loginPage.navigateToMe();
		loginPage.getLoginField().type(username);
		loginPage.getPasswordField().type(password);
		loginPage.setPageState(loginPage.getSubmitButton().click());
		try {
			return loginPage.getErrorMessage().getTextContent();
		} catch (ElementNotFoundException e) {
			return "no error message on page";
		}
	}
}
