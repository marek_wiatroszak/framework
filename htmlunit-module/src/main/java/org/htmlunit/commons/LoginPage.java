package org.htmlunit.commons;

import java.io.IOException;

import org.abstracts.products.commons.ILoginPage;
import org.htmlunit.HtmlUnitBasePage;

import com.gargoylesoftware.htmlunit.ElementNotFoundException;
import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.html.HtmlAnchor;
import com.gargoylesoftware.htmlunit.html.HtmlPasswordInput;
import com.gargoylesoftware.htmlunit.html.HtmlSpan;
import com.gargoylesoftware.htmlunit.html.HtmlTextInput;
/**
 * This class provides abstract to Taleo/Oracle login page. This page object is strictly an example.
 * Author of this page object does not own any rights to the page reflected in this abstract.
 * This page object may be deleted at any time (Use at own risk)legal agreement
 * @author nazps
 *
 */
public class LoginPage extends HtmlUnitBasePage implements ILoginPage{

	public LoginPage(WebClient _wc, String pageUrl) throws IOException {
		super(_wc, pageUrl);
	}

	public HtmlUnitBasePage navigateToMe() {
		try {
			LegalAgreementPage lap = new LegalAgreementPage(this.wc, this.pageAddress);
			lap.navigateToMe();
			htmlPage = lap.getAcceptButton().click();
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
		return this;

	}

	HtmlTextInput getLoginField(){
		HtmlTextInput loginField = htmlPage.getHtmlElementById("dialogTemplate-dialogForm-content-login-name1");
		return loginField;
	}
	
	HtmlPasswordInput getPasswordField(){
		HtmlPasswordInput passwordField = htmlPage.getHtmlElementById("dialogTemplate-dialogForm-content-login-password");
		return passwordField;
	}
	
	HtmlAnchor getSubmitButton(){
		HtmlAnchor submitButton = htmlPage.getHtmlElementById("dialogTemplate-dialogForm-content-login-defaultCmd");
		return submitButton;
	}
	
	public HtmlSpan getErrorMessage() {
		HtmlSpan actualErrorMessage = htmlPage
				.getHtmlElementById("dialogTemplate-dialogForm-content-login-errorMessageContent");
		return actualErrorMessage;
	}

	@Override
	public boolean isLoginFieldPresent() {
		boolean isLoginPresent = false;
		try{
			getLoginField();
			isLoginPresent = true;
			} catch (ElementNotFoundException notFound){
				// no op if it's not present then it's not present
			}
		return isLoginPresent;
	}

	@Override
	public boolean isPasswordPresent() {
		boolean isPasswordPresent = false;
		try{
			getPasswordField();
			isPasswordPresent = true;
			} catch (ElementNotFoundException notFound){
				// no op if it's not present then it's not present
			}
		return isPasswordPresent;
	}

	@Override
	public boolean isSubmitButtonPresent() {
		boolean isSubmitPresent = false;
		try{
			getSubmitButton();
			isSubmitPresent = true;
			} catch (ElementNotFoundException notFound){
				// no op if it's not present then it's not present
			}
		return isSubmitPresent;
	}
}
