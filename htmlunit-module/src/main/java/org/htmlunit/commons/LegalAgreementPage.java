package org.htmlunit.commons;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.abstracts.products.commons.ILegalAgreementPage;
import org.htmlunit.HtmlUnitBasePage;

import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.html.DomElement;
import com.gargoylesoftware.htmlunit.html.DomNodeList;
import com.gargoylesoftware.htmlunit.html.DomText;
import com.gargoylesoftware.htmlunit.html.HtmlAnchor;
import com.gargoylesoftware.htmlunit.html.HtmlElement;
import com.gargoylesoftware.htmlunit.html.HtmlScript;
import com.gargoylesoftware.htmlunit.html.HtmlSpan;

/**
 * This class provides abstract to Taleo/Oracle legal agreement page. This page
 * object is strictly an example. Author of this page object does not own any
 * rights to the page reflected in this abstract. This page object may be
 * deleted at any time (Use at own risk)
 * 
 * @author nazps
 * 
 */
public class LegalAgreementPage extends HtmlUnitBasePage implements
		ILegalAgreementPage {

	public LegalAgreementPage(WebClient _wc, String pageUrl) throws IOException {
		super(_wc, pageUrl);

	}

	@SuppressWarnings("unchecked")
	public List<String> getButtonTexts() {
		List<DomText> buttons = (List<DomText>) htmlPage
				.getByXPath("//span[contains(@class, 'nav-btn')]/text()");
		List<String> buttonTexts = new ArrayList<String>();
		for (DomText button : buttons) {
			buttonTexts.add(button.getWholeText());
		}
		return buttonTexts;
	}

	public void clickRejectButton() throws IOException {
		HtmlAnchor rejectButton = htmlPage
				.getHtmlElementById("dialogTemplate-dialogForm-content-legalAgreement-refuseCmd");

		this.setPageState(rejectButton.click());
	}

	public void clickAcceptButton() throws IOException {
		this.setPageState(getAcceptButton().click());

	}

	public String getErrorMessage() {
		HtmlSpan actualErrorMessage = htmlPage
				.getHtmlElementById("dialogTemplate-dialogForm-content-legalAgreement-errorMessageBox-errorMessages");
		return actualErrorMessage.getTextContent();
	}

	public List<String> getAllScripts() throws IOException {
		DomNodeList<DomElement> scripts = htmlPage
				.getElementsByTagName("script");
		List<String> scriptNames = new ArrayList<String>();
		for (DomElement script : scripts) {
			HtmlScript im = (HtmlScript) script;
			scriptNames.add(im.getSrcAttribute());

		}
		System.out.println(scriptNames.toString());
		return scriptNames;

	}

	public HtmlElement getAcceptButton() {
		HtmlAnchor acceptButton = htmlPage
				.getHtmlElementById("dialogTemplate-dialogForm-content-legalAgreement-acceptCmd");
		return acceptButton;
	}
}
