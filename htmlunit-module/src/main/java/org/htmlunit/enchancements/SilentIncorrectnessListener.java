package org.htmlunit.enchancements;

import com.gargoylesoftware.htmlunit.IncorrectnessListener;

public class SilentIncorrectnessListener
    implements IncorrectnessListener
{

	public void notify(String message, Object origin) {
		// no op
		
	}
    
	
}
