package org.testengine.htmlunit;

import java.io.IOException;

import org.abstracts.IEngine;
import org.abstracts.products.commons.ILegalAgreementPage;
import org.abstracts.products.commons.ILoginPage;
import org.htmlunit.commons.LegalAgreementPage;
import org.htmlunit.commons.LoginPage;
import org.htmlunit.engine.HtmlUnitEngine;
import org.testengine.BasePropertyHandler;
import org.testengine.PageNotSupportedException;
import org.testengine.PageObjectsFactory;


public class HtmlPageObjectsFactory extends PageObjectsFactory {
	BasePropertyHandler propertyHandler;
	public HtmlPageObjectsFactory() throws IOException{
		 propertyHandler = BasePropertyHandler.getInstance();
	}	

	
	@Override
	public ILegalAgreementPage getLegalAgreementPage(IEngine pEngine) throws PageNotSupportedException {
		HtmlUnitEngine engine = (HtmlUnitEngine) pEngine; 
		try {
			return new LegalAgreementPage(engine.getWebClient(), propertyHandler.getProperty("applicationHost"));
		} catch (IOException e) {
			e.printStackTrace();
			throw new PageNotSupportedException("HtmlUnitEngine does not support Legal agreement page");
		}
	}

	@Override
	public ILoginPage getLoginPage(IEngine pEngine) throws PageNotSupportedException {
		HtmlUnitEngine engine = (HtmlUnitEngine) pEngine; 
		try {
			return new LoginPage(engine.getWebClient(), propertyHandler.getProperty("applicationHost"));
		} catch (IOException e) {
			e.printStackTrace();
			throw new PageNotSupportedException("HtmlUnitEngine does not support Login page");
		}
	}

}
