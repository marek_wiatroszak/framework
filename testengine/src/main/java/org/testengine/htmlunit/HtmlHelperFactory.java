package org.testengine.htmlunit;

import java.io.IOException;

import org.abstracts.IEngine;
import org.abstracts.products.commons.helpers.ILoginHelper;
import org.htmlunit.commons.LoginHelper;
import org.htmlunit.engine.HtmlUnitEngine;
import org.testengine.BasePropertyHandler;
import org.testengine.HelperFactory;
import org.testengine.PageNotSupportedException;

public class HtmlHelperFactory extends HelperFactory{
	BasePropertyHandler propertyHandler;
	public HtmlHelperFactory() throws IOException{
		 propertyHandler = BasePropertyHandler.getInstance();
	}	

	@Override
	public ILoginHelper getLoginHelper(IEngine pEngine) throws PageNotSupportedException {
		HtmlUnitEngine engine = (HtmlUnitEngine) pEngine; 
		try{
		return new LoginHelper(engine.getWebClient(), propertyHandler.getProperty("applicationHost"));
		}catch (IOException e) {
			e.printStackTrace();
			throw new PageNotSupportedException("HtmlUnitEngine does not support LoginHelper");
		}
	}

}
