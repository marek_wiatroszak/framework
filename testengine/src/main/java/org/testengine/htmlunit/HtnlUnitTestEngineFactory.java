package org.testengine.htmlunit;

import org.htmlunit.engine.HtmlUnitEngine;
import org.testengine.TestEngineFactory;

public class HtnlUnitTestEngineFactory extends TestEngineFactory {
	HtmlUnitEngine engine;
	private static volatile HtnlUnitTestEngineFactory instance = null;

	public static HtnlUnitTestEngineFactory getInstance(){
		if(instance == null){
			synchronized (HtnlUnitTestEngineFactory.class){
				if(instance == null){
					instance = new HtnlUnitTestEngineFactory();
				}
			}
		}
		return instance;
	}
	
	@Override
	public HtmlUnitEngine getEngine() {
		if (engine == null){
			engine = new HtmlUnitEngine();
		}
		return engine;
	}

}
