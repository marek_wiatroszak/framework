package org.testengine;

public class PageNotSupportedException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public PageNotSupportedException(String message){
		super(message);
	}
}
