package org.testengine;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.abstracts.IPropertyHandler;

public class BasePropertyHandler implements IPropertyHandler {

	private static volatile BasePropertyHandler instance = null;
	private static Properties props;
 
    public static BasePropertyHandler getInstance() throws IOException {
        if (instance == null) {
            synchronized (BasePropertyHandler.class) {
                if (instance == null) {
                    instance = new BasePropertyHandler();
                }
            }
        }
        return instance;
    }
 
    private BasePropertyHandler() throws IOException {
    	props = new Properties();
    	loadPropertiesFromFile("default.properties");
    	
    }

	public void loadPropertiesFromFile(String fileName) throws IOException {
		Properties prop = new Properties();
		System.out.println("loading props from: " + fileName);
		ClassLoader loader = Thread.currentThread().getContextClassLoader();           
		InputStream stream = loader.getResourceAsStream(fileName);
		prop.load(stream);
		System.out.println("props loaded");
		props.putAll(prop);
		System.out.println(props);
	}
	

	public String getProperty(String propertyName) {
		if (!(System.getProperty(propertyName) == null)){
			return System.getProperty(propertyName);
		}
		
		String property = props.getProperty(propertyName);
		return property;

}}
