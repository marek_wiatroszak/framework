package org.testengine;

public class NoSuchEngineException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public NoSuchEngineException(String message){
		super(message);
	}
}
