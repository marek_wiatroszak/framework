package org.testengine;

import java.io.IOException;

import org.abstracts.IEngine;
import org.abstracts.products.commons.helpers.ILoginHelper;
import org.testengine.htmlunit.HtmlHelperFactory;
import org.testengine.selenium.SeleniumHelperFactory;
import org.testengine.seleniumwd.WebDriverHelperFactory;

public abstract class HelperFactory {
	public static HelperFactory getHelperFactory() throws IOException, NoSuchEngineException {
		String testEngine = BasePropertyHandler.getInstance().getProperty(
				"testEngine");
		if (testEngine.contains("htmlUnit")) {
			return new HtmlHelperFactory();
		} else if (testEngine.contains("seleniumRCDriver")){
			return new SeleniumHelperFactory();
		}else if (testEngine.contains("webDriver")){
			return new WebDriverHelperFactory();
		}else {
			throw new NoSuchEngineException(testEngine + " not supported");
		}
		
	}
	public abstract ILoginHelper getLoginHelper(IEngine pEngine) throws PageNotSupportedException;
	
}
