package org.testengine;

import java.io.IOException;

import org.abstracts.IEngine;
import org.abstracts.products.commons.ILegalAgreementPage;
import org.abstracts.products.commons.ILoginPage;
import org.testengine.htmlunit.HtmlPageObjectsFactory;
import org.testengine.selenium.SeleniumPageObjectsFactory;
import org.testengine.seleniumwd.WebDriverPageObjectsFactory;

/**
 * This class manages which engine will be used to run tests. The {@link #getTestFactory()} returns test engine based on testEngine property defined either in the system or in default.properties file in resources.
 * 
 * @author nazps
 * 
 */
public abstract class PageObjectsFactory {

	public static PageObjectsFactory getPageObjectFactory() throws IOException, NoSuchEngineException {
		String testEngine = BasePropertyHandler.getInstance().getProperty(
				"testEngine");

		if (testEngine.contains("htmlUnit")) {
			return new HtmlPageObjectsFactory();
		} else if (testEngine.contains("seleniumRCDriver")){
			return new SeleniumPageObjectsFactory();
		}else if (testEngine.contains("webDriver")){
			return new WebDriverPageObjectsFactory();
		}else {
			throw new NoSuchEngineException(testEngine + " not supported");
		}
		
	}
	
	public abstract ILegalAgreementPage getLegalAgreementPage(IEngine engine) throws PageNotSupportedException;
	public abstract ILoginPage getLoginPage(IEngine pEngine) throws PageNotSupportedException;
}