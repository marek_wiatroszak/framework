package org.testengine.selenium;

import org.abstracts.IEngine;
import org.abstracts.products.commons.helpers.ILoginHelper;
import org.testengine.HelperFactory;
import org.testengine.PageNotSupportedException;

public class SeleniumHelperFactory extends HelperFactory {

	@Override
	public ILoginHelper getLoginHelper(IEngine pEngine)
			throws PageNotSupportedException {
		throw new PageNotSupportedException("Selenium engine doesn't support LoginHelper");
	}

}
