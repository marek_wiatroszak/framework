package org.testengine.selenium;

import org.abstracts.IEngine;
import org.selenium.SeleniumEngine;
import org.testengine.TestEngineFactory;

public class SeleniumTestEngineFactory extends TestEngineFactory {
	SeleniumEngine engine;
	private static volatile SeleniumTestEngineFactory instance = null;

	public static SeleniumTestEngineFactory getInstance(){
		if(instance == null){
			synchronized (SeleniumTestEngineFactory.class){
				if(instance == null){
					instance = new SeleniumTestEngineFactory();
				}
			}
		}
		return instance;
	}
	
	@Override
	public IEngine getEngine() {
		if(engine == null){
			engine = new SeleniumEngine();
			engine.startEngine();
		}
		return null;
	}

}
