package org.testengine.seleniumwd;

import java.io.IOException;

import org.abstracts.IEngine;
import org.abstracts.products.commons.ILegalAgreementPage;
import org.abstracts.products.commons.ILoginPage;
import org.seleniumwdmodule.commons.LegalAgreementPage;
import org.seleniumwdmodule.commons.LoginPage;
import org.seleniumwdmodule.engine.WebDriverEngine;
import org.testengine.BasePropertyHandler;
import org.testengine.PageNotSupportedException;
import org.testengine.PageObjectsFactory;

public class WebDriverPageObjectsFactory extends PageObjectsFactory {

	BasePropertyHandler propertyHandler;

	public WebDriverPageObjectsFactory() throws IOException {
		propertyHandler = BasePropertyHandler.getInstance();
	}


	
	@Override
	public ILegalAgreementPage getLegalAgreementPage(IEngine pEngine)
			throws PageNotSupportedException {
		
		WebDriverEngine engine = (WebDriverEngine) pEngine;
		return new LegalAgreementPage(engine.getDriver(),
				propertyHandler.getProperty("applicationHost"));
	}

	@Override
	public ILoginPage getLoginPage(IEngine pEngine)
			throws PageNotSupportedException {
		WebDriverEngine engine = (WebDriverEngine) pEngine;
		return new LoginPage(engine.getDriver(),
				propertyHandler.getProperty("applicationHost"));
	}

}
