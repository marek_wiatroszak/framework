package org.testengine.seleniumwd;

import java.io.IOException;

import org.abstracts.IEngine;
import org.abstracts.products.commons.helpers.ILoginHelper;
import org.seleniumwdmodule.commons.LoginHelper;
import org.seleniumwdmodule.engine.WebDriverEngine;
import org.testengine.BasePropertyHandler;
import org.testengine.HelperFactory;
import org.testengine.PageNotSupportedException;

public class WebDriverHelperFactory extends HelperFactory {

	BasePropertyHandler propertyHandler;

	public WebDriverHelperFactory() throws IOException {
		propertyHandler = BasePropertyHandler.getInstance();
	}

	@Override
	public ILoginHelper getLoginHelper(IEngine pEngine)
			throws PageNotSupportedException {
		WebDriverEngine engine = (WebDriverEngine) pEngine;
		return new LoginHelper(engine.getDriver(),
				propertyHandler.getProperty("applicationHost"));

	}
}
