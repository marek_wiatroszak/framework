package org.testengine.seleniumwd;

import java.io.IOException;

import org.seleniumwdmodule.engine.FirefoxWDEngine;
import org.seleniumwdmodule.engine.WebDriverEngine;
import org.testengine.BasePropertyHandler;
import org.testengine.TestEngineFactory;

public class WebDriverEngineFactory extends TestEngineFactory {
	WebDriverEngine engine;
	static BasePropertyHandler propertyHandler;
	private static volatile WebDriverEngineFactory instance = null;
	
	public static WebDriverEngineFactory getInstance(){
		if(instance == null){
			synchronized (WebDriverEngineFactory.class){
				if(instance == null){
					try {
						propertyHandler = BasePropertyHandler.getInstance();
						propertyHandler.loadPropertiesFromFile("selenium.properties");
						instance = new WebDriverEngineFactory();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}
		}
		return instance;
	}

	@Override
	public WebDriverEngine getEngine() {
			
			if (propertyHandler.getProperty("browser").contains("firefox")) {
				System.out.println("starting Firefox WebDriver Engine");
				engine = new FirefoxWDEngine();
			}
		return engine;
	}
}
