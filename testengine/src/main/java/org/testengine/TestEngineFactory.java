package org.testengine;

import java.io.IOException;

import org.abstracts.IEngine;
import org.testengine.htmlunit.HtnlUnitTestEngineFactory;
import org.testengine.selenium.SeleniumTestEngineFactory;
import org.testengine.seleniumwd.WebDriverEngineFactory;

public abstract class TestEngineFactory {

	public static TestEngineFactory getTestEngineFactory() throws IOException, NoSuchEngineException {
		String testEngine = BasePropertyHandler.getInstance().getProperty(
				"testEngine");
		if (testEngine.contains("htmlUnit")) {
			new HtnlUnitTestEngineFactory();
			return HtnlUnitTestEngineFactory.getInstance();
		} else if (testEngine.contains("seleniumRCDriver")){
			new SeleniumTestEngineFactory();
			return SeleniumTestEngineFactory.getInstance();
		}else if (testEngine.contains("webDriver")){
			new WebDriverEngineFactory();
			return WebDriverEngineFactory.getInstance();
		}else {
			throw new NoSuchEngineException(testEngine + " not supported");
		}
		
	}
	
	public abstract IEngine getEngine();

}
