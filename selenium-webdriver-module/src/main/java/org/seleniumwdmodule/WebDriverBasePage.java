package org.seleniumwdmodule;

import org.abstracts.IBasePage;
import org.openqa.selenium.WebDriver;

public class WebDriverBasePage implements IBasePage {
	protected WebDriver driver;
	protected String pageAddress;

	public WebDriverBasePage(WebDriver pDriver, String pPageAddress) {
		driver = pDriver;
		pageAddress = pPageAddress;
		
	}

	@Override
	public String getTitle() {
		return driver.getTitle();
	}

	@Override
	public WebDriverBasePage navigateToMe() {
		driver.get(pageAddress);
		return this;
	}

	@Override
	public String getPageAddress() {
		return pageAddress;
	}

}
