package org.seleniumwdmodule.commons;

import java.io.IOException;

import org.abstracts.products.commons.ILoginPage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.seleniumwdmodule.WebDriverBasePage;

public class LoginPage extends WebDriverBasePage implements ILoginPage {

	public LoginPage(WebDriver pDriver, String pPageAddress) {
		super(pDriver, pPageAddress);
	}

	public WebDriverBasePage navigateToMe(){

		super.navigateToMe();
		LegalAgreementPage lap = new LegalAgreementPage(driver, pageAddress);
		try {
			lap.clickAcceptButton();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return this;
		
	}
	
	@Override
	public boolean isLoginFieldPresent() {
		return getLoginInput().isDisplayed();
	}

	@Override
	public boolean isPasswordPresent() {
		return getPasswordInput().isDisplayed();
	}

	@Override
	public boolean isSubmitButtonPresent() {
		return getSubmitButton().isDisplayed();
	}

	public WebElement getLoginInput() {
		return driver.findElement(By.id("dialogTemplate-dialogForm-content-login-name1"));
	}


	public WebElement getPasswordInput() {
		return driver.findElement(By.id("dialogTemplate-dialogForm-content-login-password"));
	}

	public WebElement getSubmitButton() {
		return driver.findElement(By.id("dialogTemplate-dialogForm-content-login-defaultCmd"));
	}

	public WebElement getErrorMessage() {
		return driver.findElement(By.id("dialogTemplate-dialogForm-content-login-errorMessageContent"));
	}
}
