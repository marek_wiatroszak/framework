package org.seleniumwdmodule.commons;

import org.abstracts.products.commons.IWelcomeCentrePage;
import org.openqa.selenium.WebDriver;
import org.seleniumwdmodule.WebDriverBasePage;

public class WelcomeCentrePage extends WebDriverBasePage implements IWelcomeCentrePage {

	public WelcomeCentrePage(WebDriver pDriver, String pPageAddress) {
		super(pDriver, pPageAddress);
	}

}
