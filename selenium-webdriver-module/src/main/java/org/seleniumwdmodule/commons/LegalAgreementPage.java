package org.seleniumwdmodule.commons;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.abstracts.products.commons.ILegalAgreementPage;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.UnhandledAlertException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.seleniumwdmodule.WebDriverBasePage;

public class LegalAgreementPage extends WebDriverBasePage implements
		ILegalAgreementPage {
	public LegalAgreementPage(WebDriver pDriver, String pPageAddress) {
		super(pDriver, pPageAddress);

	}

	@Override
	public List<String> getButtonTexts() {
		List<WebElement> buttons = this.driver.findElements(By
				.xpath("//span[contains(@class, 'nav-btn')]"));
		List<String> buttonTexts = new ArrayList<String>();
		for (WebElement element : buttons) {
			buttonTexts.add(element.getText());
		}
		return buttonTexts;
	}

	@Override
	public void clickRejectButton() throws IOException {
		WebElement rejectButton = this.driver
				.findElement(By
						.id("dialogTemplate-dialogForm-content-legalAgreement-refuseCmd"));
		rejectButton.click();
	}

	@Override
	public void clickAcceptButton() throws IOException {
		try {
			Alert alert = this.driver.switchTo().alert();
			alert.accept();
			System.out.println(alert.getText());
		} catch (NoAlertPresentException e) {
		} catch (UnhandledAlertException e1) {
			// TODO need to handle modal window hereor alert
			
		}
		WebElement acceptButton = this.driver
				.findElement(By
						.id("dialogTemplate-dialogForm-content-legalAgreement-acceptCmd"));
		acceptButton.click();
	}

	@Override
	public String getErrorMessage() {
		WebElement errorMessageElement = this.driver
				.findElement(By
						.id("dialogTemplate-dialogForm-content-legalAgreement-errorMessageBox-errorMessages"));
		return errorMessageElement.getText();
	}

	@Override
	public List<String> getAllScripts() throws IOException {
		List<String> scriptNames = new ArrayList<String>();
		List<WebElement> scripts = this.driver.findElements(By
				.tagName("script"));
		for (WebElement script : scripts) {
			scriptNames.add(script.getAttribute("src"));
		}
		return scriptNames;
	}

}
