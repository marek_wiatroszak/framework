package org.seleniumwdmodule.commons;

import java.io.IOException;

import org.abstracts.products.commons.helpers.ILoginHelper;
import org.openqa.selenium.WebDriver;
import org.seleniumwdmodule.WebDriverBaseHelper;

public class LoginHelper extends WebDriverBaseHelper implements ILoginHelper {

	LoginPage loginPage;
	String basePageAddres;
	public LoginHelper(WebDriver pDriver, String pBasePageUrl) {
		super(pDriver, pBasePageUrl);
		this.basePageAddres = pBasePageUrl;
		loginPage = new LoginPage(pDriver, basePageAddres);

	}

	@Override
	public WelcomeCentrePage login(String userName, String password)
			throws IOException {
		
		driver.manage().deleteAllCookies();
		loginPage.navigateToMe();
		loginPage.getLoginInput().sendKeys(userName);
		loginPage.getPasswordInput().sendKeys(password);
		loginPage.getSubmitButton().click();
		WelcomeCentrePage wcp = new WelcomeCentrePage(driver, basePageAddres);
		return wcp;
	}

	@Override
	public String invalidLogin(String userName, String password)
			throws IOException {
		driver.manage().deleteAllCookies();
		loginPage.navigateToMe();
		loginPage.getLoginInput().sendKeys(userName);
		loginPage.getPasswordInput().sendKeys(password);
		loginPage.getSubmitButton().click();
		
		return loginPage.getErrorMessage().getText();
	}

}
