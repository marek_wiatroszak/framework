package org.seleniumwdmodule.engine;

import java.util.concurrent.TimeUnit;

import org.abstracts.IEngine;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class FirefoxWDEngine extends WebDriverEngine implements IEngine {
	WebDriver webDriver;

	@Override
	public void startEngine() {
		webDriver = new FirefoxDriver();
		webDriver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
		isStarted = true;
	}

	@Override
	public void clearSessionData() {
		
		webDriver.manage().deleteAllCookies();
	}

	@Override
	public void stopEngine() {
		webDriver.quit();
		isStarted = false;

	}

	@Override
	public WebDriver getDriver() {
		return webDriver;
	}

	@Override
	public boolean isStarted() {
		// TODO Auto-generated method stub
		return false;
	}

}
