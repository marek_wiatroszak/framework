package org.seleniumwdmodule.engine;

import org.abstracts.IEngine;
import org.openqa.selenium.WebDriver;

public abstract class WebDriverEngine implements IEngine{
	protected boolean isStarted = false;
	public abstract WebDriver getDriver();


}
