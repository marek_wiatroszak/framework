package org.abstracts;


public interface IPropertyHandler {

	String getProperty(String propertyName);
}
