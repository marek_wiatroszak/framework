package org.abstracts.products.commons.helpers;

import java.io.IOException;

import org.abstracts.products.commons.IWelcomeCentrePage;

public interface ILoginHelper {

	public IWelcomeCentrePage login(String userName, String password) throws IOException;
	public String invalidLogin(String userName, String password) throws IOException;
}
