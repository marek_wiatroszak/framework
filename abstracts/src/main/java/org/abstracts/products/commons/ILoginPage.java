package org.abstracts.products.commons;

import org.abstracts.IBasePage;

public interface ILoginPage extends IBasePage {

	boolean isLoginFieldPresent();

	boolean isPasswordPresent();

	boolean isSubmitButtonPresent();

}
