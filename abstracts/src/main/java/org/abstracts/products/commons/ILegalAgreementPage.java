package org.abstracts.products.commons;

import java.io.IOException;
import java.util.List;

import org.abstracts.IBasePage;

public interface ILegalAgreementPage extends IBasePage{
	public List<String> getButtonTexts();
	public void clickRejectButton() throws IOException;
	public void clickAcceptButton() throws IOException;
	public String getErrorMessage();
	public List<String> getAllScripts() throws IOException;
}
