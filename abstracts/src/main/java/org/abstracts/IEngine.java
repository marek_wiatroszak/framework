package org.abstracts;

public interface IEngine {

	public void startEngine();
	public void clearSessionData();
	public void stopEngine();
	public boolean isStarted();
}
