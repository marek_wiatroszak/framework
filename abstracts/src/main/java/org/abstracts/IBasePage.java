package org.abstracts;



/**
 * This is the contract for all pages in the framework
 * @author nazps
 *
 */
public interface IBasePage {
	String getTitle();
	IBasePage navigateToMe();
	String getPageAddress();
	
}
